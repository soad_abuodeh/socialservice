<html>
	<head>
		<style>
			ul {
				list-style-type: none;
				margin: 0;
				padding: 0;
				overflow: hidden;
				background-color: #333;
			}

			li {
				float: left;
			}

			li a {
				display: block;
				color: white;
				text-align: center;
				padding: 14px 16px;
				text-decoration: none;
			}

			li a:hover:not(.active) {
				background-color: #969edc;
			}

			.active {
				background-color: #969edc;
				
			}
		</style>
	</head>
	<body>

		<ul>
			  <li><a class="active" href="<?php echo site_url('UsersController/index');?>">Home</a></li>
			  <li><a href="#news">News</a></li>
			  <li><a href="#contact">Contact</a></li>
			  <li><a href="#about">About</a></li>
			  <?php 
				  if(isset($_SESSION['email'])){
			  ?>
					<li style="float:right"><a href="<?php echo site_url('UsersController/logOut');?>">Log out</a></li>
			  <?php
				  } 
			  ?>
		</ul>

	</body>
</html>

