<html>
	<head>
		<title>Log in</title>
		<?php $this->load->helper('url'); ?>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/login.css">
	</head>
	<body>
		<div>
			<?php
				echo form_open('UsersController/login');
			?>
			<h3>Email </h3>
			<input name="email" class="input" type="email" placeholder="Please Enter your Email" required>
			<h3>Password </h3>
			<input name="password" class="input" type="password" required>
			<br/>
			<br/>
			<center>
				<input id="submit"value="Login" type="submit" >
			</center>
			
			<?php
				echo form_close();
			?>
			<?php
				echo form_open('UsersController/registerView');
			?>
			<center>
				<input id="submit"value="Sign Up" type="submit" >
			</center>
			<?php
				echo form_close();
			?>
			<h4><u>
			<?php 
				if($this->session->flashdata('msg')){
					 echo $this->session->flashdata('msg'); 
				}
			?>
			</u></h4>
		</div>
	</body>
</html>