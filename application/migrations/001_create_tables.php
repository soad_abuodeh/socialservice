<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_Tables extends CI_Migration {

 public function up()
	{
		$this->load->dbforge();
		$this->dbforge->add_field(array(
			  '`id` int(11) NOT NULL AUTO_INCREMENT',
			  '`email` varchar(50) NOT NULL',
			  '`password` varchar(50) NOT NULL',
			  '`name` varchar(50) NOT NULL',
			  'PRIMARY KEY (`id`)'
					));
		$this->dbforge->add_key('id');
		$this->dbforge->create_table('information');
		
		$this->load->dbforge();
		$this->dbforge->add_field(array(
			  '`id` int(11) NOT NULL AUTO_INCREMENT',
			  '`post` varchar(100) NOT NULL',
			  '`check_image` int(1) NOT NULL DEFAULT 0',
			  '`image` varchar(50) NOT NULL',
			  '`user_id` int(11) NOT NULL',
			  '`time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
			  'PRIMARY KEY (`id`)'
					));
		$this->dbforge->add_key('id');
		$this->dbforge->create_table('posts');
		
		
		$this->load->dbforge();
		$this->dbforge->add_field(array(
			  'id int(11) NOT NULL AUTO_INCREMENT',
			  'comment varchar(100) NOT NULL',
			  'user_id int(11) NOT NULL',
			  'post_id int(11) NOT NULL',
			  'time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
			  'PRIMARY KEY (`id`)',
			  'UNIQUE KEY `id` (`id`)'
		));
		$this->dbforge->add_key('id');
		$this->dbforge->create_table('comments');
		
	}

	public function down()
	{
		  $this->load->dbforge();
		$this->dbforge->drop_table('users');
	}

}

?>